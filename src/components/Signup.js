import { useState } from 'react'
import { useFormik } from 'formik'
import {
    TextField,
    Button,
    Typography,
    IconButton,
    CircularProgress,
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { Visibility, VisibilityOff } from '@material-ui/icons'
import axios from 'axios'
import { object, string } from 'yup'
import _ from 'lodash'

const useStyles = makeStyles((theme) => ({
    form: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'stretch',
    },
    row: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        margin: theme.spacing(1),
    },
    input: {
        width: '50%',
    },
    button: {
        margin: theme.spacing(1),
    },
}))

const validationSchema = object().shape({
    username: string()
        .required('Required')
        .min(4, 'Username must be at least 4 characters')
        .max(50, 'Username must be less than 50 characters')
        .matches(/^[a-zA-Z]+\w+$/, 'Username cannot start with symbols'),
    password: string().required('Required').min(8, 'Password is too weak'),
})

const cleanup = (data) => {
    return _.omitBy(data, _.isNil)
}

const Signup = () => {
    const classes = useStyles()

    const onSubmit = async (values, actions) => {
        try {
            await axios.post(
                'https://jsonplaceholder.typicode.com/posts',
                cleanup(values)
            )

            actions.resetForm()
            alert('Data has been submitted!')
        } catch (error) {
            alert(`Error: ${error.message}`)
        }
    }

    const formik = useFormik({
        initialValues: {
            username: '',
            password: '',
        },
        onSubmit,
        validationSchema,
    })

    return (
        <form className={classes.form} onSubmit={formik.handleSubmit}>
            <div className={classes.row}>
                <MyInput
                    label="Username"
                    placeholder="Alphanumeric"
                    name="username"
                    formik={formik}
                />
            </div>
            <div className={classes.row}>
                <MyInput
                    label="Password"
                    name="password"
                    password
                    formik={formik}
                />
            </div>
            <div className={classes.row}>
                {formik.isSubmitting ? (
                    <CircularProgress />
                ) : (
                    <Button
                        className={classes.button}
                        type="submit"
                        variant="contained"
                        disabled={!formik.isValid}
                        color="primary"
                    >
                        Submit
                    </Button>
                )}
            </div>
        </form>
    )
}

const MyInput = ({ password, type = 'text', formik, name, ...props }) => {
    const [visible, setVisible] = useState(false)
    const toggleVisible = () => setVisible(!visible)

    return (
        <>
            <TextField
                id={name}
                variant="outlined"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values[name]}
                type={password && !visible ? 'password' : type}
                fullWidth
                InputProps={{
                    endAdornment: password ? (
                        <IconButton onClick={toggleVisible}>
                            {visible ? <Visibility /> : <VisibilityOff />}
                        </IconButton>
                    ) : null,
                }}
                {...props}
            />
            {formik.touched[name] && formik.errors[name] ? (
                <Typography variant="body2" color="error">
                    {formik.errors[name]}
                </Typography>
            ) : null}
        </>
    )
}

export default Signup
